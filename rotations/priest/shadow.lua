local dynEval = NePCR.dynEval

local PeFetch = function(setting)
	local key = NeP.Interface.fetchKey('NePConfPriestShadow',setting)
    return key
end

local addonColor = '|cff'..NeP.Interface.addonColor

local InsanityHack = GetSpellInfo(15407)

local config = {
	key = 'NePConfPriestShadow',
	profiles = true,
	title = '|T'..NeP.Interface.Logo..':10:10|t'..NeP.Info.Nick..' Config',
	subtitle = 'Priest Shadow Settings',
	color = 'FFFFFF',
	width = 250,
	height = 500,
	config = {
		-- Keybinds
		{type = 'header', text = addonColor..'Keybinds:', align = 'center'},
			-- Control
			{type = 'text', text = addonColor..'Control: ', align = 'left', size = 11, offset = -11},
			{type = 'text', text = 'Mind Sear', align = 'right', size = 11, offset = 0 },
			-- Shift
			{type = 'text', text = addonColor..'Shift:', align = 'left', size = 11, offset = -11},
			{type = 'text', text = 'Cascade', align = 'right', size = 11, offset = 0 },
			-- Alt
			{type = 'text', text = addonColor..'Alt:',align = 'left', size = 11, offset = -11},
			{type = 'text', text = 'Pause Rotation', align = 'right', size = 11, offset = 0 },
		
		-- [[ General Settings ]]
		{type = 'spacer'},{type = 'rule'},
		{type = 'header', text = addonColor..'General', align = 'center'},
			{ type = 'checkbox', text = 'Move faster', key = 'canMoveF', default = true },

			{type = "spacer"},{type = 'rule'},
        {
            type = "checkbox",
            default = false,
            text = "Use Speed Booster Outside Combat",
            key = "speed3",
            desc = "Will make use of Body and Soul and Angelic Feather outside of combat."
        },
		
		-- [[ Survival settings ]]
		{type = 'spacer'},{type = 'rule'},
		{type = 'header', text = addonColor..'Survival', align = 'center'},
			{ type = "spinner", text = "Flash Heal", key = "FlashHeal", default = 35},
	}
}

NeP.Interface.buildGUI(config)

local lib = function()
	NePCR.Splash()
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('NePConfPriestShadow') end)
end

local keybinds = {
	-- Pause
	{'pause', 'modifier.alt'},
	-- Cascade
	{'127632', 'modifier.shift'},
	-- Mind Sear
	{'48045', 'modifier.control'},
}

local Buffs = {
	-- Power Word: Fortitude
	{'21562', '!player.buffs.stamina'},
	-- Shadowform
	{'15473', 'player.stance != 1'},
	--{'1706', 'player.falling'} -- Levitate

}

local Cooldowns = {
	-- Mindbender
	{'123040'},
	 --Shadowfiend
	{'34433'},
	-- Vampiric Embrace
	{'15286', '@coreHealing.needsHealing(65, 3)'}
}

local Survival = {
	-- PW:Shield
	{'17', '!player.buff(17)', 'player'},
	 -- Flash Heal
	{'2061', (function() return dynEval('player.health < '..PeFetch('FlashHeal')) end), 'player'},
}

local AoE = {
	-- Cascade
	{'!127632', {'talent(6, 1)', 'player.spell(127632).cooldown = 0'}},
	-- Mind Sear
	{'48045', 'target.area(8).enemies >= 3'},

	{{
		{'!2944', {'!player.buff(132573)', 'player.shadoworbs >= 3'}},
		{{
			{'!48045', 'target.debuff(179338).duration <= 1'},
			{'48045'}
		}, 'player.buff(132573)'}
	}, 'talent(3, 3)'},
}

local Moving = {
	-- Shadow Word: Death
	{'32379', '@NePCR.instaKill(20)'},
	-- Shadow Word: Pain.
	{'589', '@NePCR.aDot(2)'},
}

local inCombat = {

	{"pause", (function()
		if UnitCastingInfo("player") == GetSpellInfo(32375) then return true end
		return false
	end)},

	{"17", {"!player.debuff(6788)", "!talent(2, 1)", "player.glyph(33202)"}, "player"},

	-- Void Entropy
	{'155361', {'player.shadoworbs >= 3', '!target.debuff(155361)'}},

	-- Cast Devouring Plague with 3 or more Shadow Orbs.
	{'2944', {'player.shadoworbs >= 3', '!target.debuff(2944)'}},

	-- Cast Mind Blast if you have fewer than 5 Shadow Orbs.
	{'!8092', 'player.shadoworbs < 5', 'target'},

	-- Cast Shadow Word: Death if you have fewer than 5 Shadow Orbs.
	{'!32379', {'player.shadoworbs < 5', '@NePCR.instaKill(20)'}},

	-- Cast Insanity on the target when you have the Insanity buff (if you are using the Insanity talent).
	{'/cast '..InsanityHack, 'player.buff(132573).duration > 0.4'},

	

	-- Apply and maintain Shadow Word: Pain.
	        {'589', {'!target.debuff(589)', 'player.shadoworbs >= 4'}},

	-- Apply and maintain Vampiric Touch.
	-- Vampiric Touch (Focus)
						{'34914', {
							'!lastcast(589)',
							'lastcast(34914)',
							'target.debuff(34914)',
							'player.shadoworbs >= 4',
							'target.debuff(589).duration > 5'
						}},

					
-- Cast Mind Spike if you have a Surge of Darkness proc (if you are using this talent).
	--{'73510', 'player.buff(Surge of Darkness)'},
	{'73510', 'target.health >= 20'},

	{AoE, 'player.area(40).enemies >= 3'},

	-- Angelic Feather
    {'121536', {
            'talent(2, 2)',
            'player.moving',
            '!player.buff(121557)',
            'player.spell(121536).charges >= 1',
            (function() return PeFetch('canMoveF') end)},
        'player.ground'},

	-- Cast Mind Flay as your filler spell.
	{'15407'}

} 

local outCombat = {
	{"", (function()
		if not firstRun then
			if FireHack then
				LineOfSight = nil
				function LineOfSight(a, b)
					local ignoreLOS = {[76585]=true,[77063]=true,[86252]=true,[83745]=true,[77182]=true,[81318]=true,[78981]=true,[86644]=true,[77252]=true,[79504]=true,[77891]=true,[77893]=true}
					local losFlags =  bit.bor(0x10, 0x100)
					local ax, ay, az = ObjectPosition(a)
					local bx, by, bz = ObjectPosition(b)
					local aCheck = select(6,strsplit("-",UnitGUID(a)))
					local bCheck = select(6,strsplit("-",UnitGUID(b)))
					
					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end
					if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then return false end
					return true
				end
			end
			
			-- Run once after unlocking
			if not not ProbablyEngine.protected.unlocked
				and ProbablyEngine.protected.method == "firehack" then firstRun = true end
		end
	end)},
	{Buffs},
	{keybinds},
	 {{
            -- Body and Soul
          --  {"17", {"talent(2, 1)", "!player.debuff(6788)", "player.moving", (function() return fetch("speed1") end)}},
            
            -- Angelic Feather
            {'!121536', {
                    'talent(2, 2)',
                    'player.moving',
                    '!player.buff(121557)',
                    'player.spell(121536).charges >= 1',
                    (function() return PeFetch('canMoveF') end)},
                'player.ground'}
        }, (function() return PeFetch('speed3') end)},
}

NeP.Engine.registerRotation(258, 'Priest - Shadow', 
	{-- In-Combat
		{keybinds},
		{Buffs},
		{Moving, 'player.moving'},
		{Survival, 'player.health < 100'},
		{Cooldowns, 'modifier.cooldowns'},
		{inCombat},
	}, outCombat, lib)