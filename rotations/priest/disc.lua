local Fetch = NeP.Interface.fetchKey
local dynEval = NePCR.dynEval

local config = {

	key = 'NeP_PriestDisc',
	title = 'Priest Discipline',
	subtitle = 'Settings',
	color = 'FFFFFF',
	width = 180,
	height = 250,
	profiles = true,
	resize = false,
	config = {

		-- Player
		{type = 'header', text = 'Player Settings:', align = 'center'},
			{type = 'spinner', text = 'Desperate Prayer', key = 'DesperatePrayer', default = 40},

		-- Raid
		{type = 'spacer'},{type = 'rule'},
		{type = 'header', text = 'Raid Settings:', align = 'center'},
			{type = 'spinner', text = 'Flash Heal', key = 'FlashHeal', default = 50},
			{type = 'spinner', text = 'Penance', key = 'Penance', default = 80},
			{type = 'spinner', text = 'Saving Grace', key = 'SavingGrace', default = 30},
			{type = 'spinner', text = 'Heal', key = 'Heal', default = 100},

		-- Tank
		{type = 'spacer'},{type = 'rule'},
		{type = 'header', text = 'Tank Settings:', align = 'center'},
			{type = 'spinner', text = 'Saving Grace', key = 'SavingGraceTank', default = 30},

	}
}

NeP.Interface.buildGUI(config)

local init = function()
	NeP.Interface.CreateToggle(
		'AgressiveMode',
		'Turn this on to focus on DPS.\nThis lasts until lowest is bellow 60% health', 
		'Interface\\Icons\\Ability_priest_atonement.png')
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('NeP_PriestDisc') end)
end


local Shared = {
	
	-- Buff
	{ 'Power Word: Fortitude', '!player.aura(stamina)', 'player' },

}

local Cooldowns = {
	{'Pain Suppression', 'tank.health < 30', 'tank' },
	{'Power Infusion', '!player.mana >= 30', 'player'},
	{'Power Infusion', 'AoEHeal(50, 3)', 'player'},
}

local Atonement = {
	{'Holy Fire', nil, 'target'}, 
	{'Penance', nil, 'target'},
	{'Smite', nil, 'target'},
}

local AoE = {
	{'Cascade', 'AoEHeal(90, 3)', 'player'},
	{'Holy Nova', '@NePCR.HolyNova(3)', 'player'},
	{'Prayer of Healing', {'toggle.AoE', 'AoEHeal(90, 3)'}, 'player'},
}

local inCombat = {

	-- Shared stuff
	{ Shared },

	{'Purify', 'player.dispellAll(Purify)', 'player'},
	{'Archangel', 'player.buff(Evangelism).count >= 5', 'player'},
	{'!Saving Grace',{ 
		(function() return dynEval('lowest.health < '..Fetch('NeP_PriestDisc', 'SavingGrace', 30)) end),
		'or',
		(function() return dynEval('lowest.health < '..Fetch('NeP_PriestDisc', 'SavingGraceTank', 40)) end)
	}, 'lowest'},
	{'Desperate Prayer', (function() return dynEval('player.health < '..Fetch('NeP_PriestDisc', 'DesperatePrayer', 40)) end), 'player'},
	{Cooldowns, 'toggle.Cooldowns'},
	{Atonement, {
		'target.exists',
		'toggle.AgressiveMode', 
		'!lowest.health <= 60'
	}},
	{'Flash Heal', {'player.buff(Surge of Light)', 'lowest.health <= 90'}, 'lowest'},
	{'Penance', (function() return dynEval('lowest.health < '..Fetch('NeP_PriestDisc', 'Penance', 80)) end), 'lowest'},
	{'Power Word: Shield', {'lowest.health < 35', '!lowest.debuff(Weakened Soul)'}, 'lowest'},
	{'Flash Heal', (function() return dynEval('lowest.health < '..Fetch('NeP_PriestDisc', 'FlashHeal', 50)) end), 'lowest'},
	{AoE},
	{'Power Word: Solace', nil, 'target'},
	{Atonement, {
		'target.exists',
		'!player.buff(Evangelism).count >= 5', 
		'player.mana >= 20',
		'target.range <= 30',
		'target.infront'
	}},
	{'Penance', 'lowest.health < 100', 'lowest'},
	{'Power Word: Shield', {'lowest.health < 60', '!lowest.debuff(Weakened Soul)'}, 'lowest'},
	{'Heal', (function() return dynEval('lowest.health < '..Fetch('NeP_PriestDisc', 'Heal', 100)) end), 'lowest'},

}

local outCombat = {

	-- Shared stuff
	{Shared},
}

NeP.Engine.registerRotation(256, 'Priest - Discipline', inCombat, outCombat, init)
NeP.Engine.registerRotation(256, 'Priest - Discipline test', inCombat, outCombat)