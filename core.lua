NePCR = {}

local Parse = NeP.DSL.parse
local Fetch = NeP.Interface.fetchKey

-- Temp Hack
function NePCR.Splash()
	NeP.Interface.CreateToggle(
		'ADots',
		'Click here to dot all the things!',
		'Interface\\Icons\\Ability_creature_cursed_05.png')
end

function NePCR.dynEval(condition, spell)
	return Parse(condition, spell or '')
end

NeP.library.register('NePCR', {

	HolyNova = function(units)
		local minHeal = GetSpellBonusDamage(2) * 1.125
		local total = 0
		for i=1,#NeP.OM.unitFriend do
			local Obj = NeP.OM.unitFriend[i]
			if Obj.distance <= 12 then
				if max(0, Obj.maxHealth - Obj.actualHealth) > minHeal then
					total = total + 1
				end
			end
		end
		return total > units
	end,

	instaKill = function(health)
		local Spell = NeP.Engine.Current_Spell
		if NeP.DSL.Conditions['toggle']('ADots') then
			for i=1,#NeP.OM.unitEnemie do
				local Obj = NeP.OM.unitEnemie[i]
				if NeP.DSL.Conditions['health'](Obj.key) <= health then
					if IsSpellInRange(Spell, Obj.key)
					and NeP.Protected.Infront('player', Obj.key)
					and UnitCanAttack('player', Obj.key) then
						NeP.Protected.Macro('/target '..Obj.key)
						return true
					end
				end
			end
		else
			if  NeP.DSL.Conditions['health']('target') <= health then
				if IsSpellInRange(Spell, 'target')
				and NeP.Protected.Infront('player', 'target')
				and UnitCanAttack('player', 'target') then
					return true
				end
			end
		end
		return false
	end,

	aDot = function(refreshAt)
		
		if not IsUsableSpell(Spell) then return false end
		local Spell = NeP.Engine.Current_Spell
		local _,_,_, SpellcastingTime = GetSpellInfo(Spell)
		local SpellcastingTime = SpellcastingTime * 0.001

		if NeP.DSL.Conditions['toggle']('ADots') then
			for i=1,#NeP.OM.unitEnemie do
				local Obj = NeP.OM.unitEnemie[i]
				if (UnitAffectingCombat(Obj.key) or Obj.is == 'dummy') then
					local _,_,_,_,_,_,debuffDuration = UnitDebuff(Obj.key, Spell, nil, 'PLAYER')
					if not debuffDuration or debuffDuration - GetTime() < refreshAt then
						if UnitCanAttack('player', Obj.key)
						and NeP.Protected.Infront('player', Obj.key)
						and IsSpellInRange(Spell, Obj.key) then				
							if NeP.DSL.Conditions['ttd'](Obj.key) > ((debuffDuration or 0) + SpellcastingTime)
							or SpellcastingTime < 1 then
								NeP.Engine.ForceTarget = Obj.key
								return true
							end
						end
					end
				end
			end
		else
			local _,_,_,_,_,_,debuffDuration = UnitDebuff('target', Spell, nil, 'PLAYER')
			if not debuffDuration or debuffDuration - GetTime() < refreshAt then
				if IsSpellInRange(Spell, 'target')
				and NeP.Protected.Infront('player', 'target')
				and UnitCanAttack('player', 'target') then
					if NeP.DSL.Conditions['ttd']('target') > ((debuffDuration or 0) + SpellcastingTime)
					or SpellcastingTime < 1 then

						return true
					end
				end
			end
		end
		return false
	end

})

NeP.DSL.RegisterConditon("petinmelee", function(target)
   if FireHack then 
		return NeP.Core.Distance('pet', target) < (UnitCombatReach('pet') + UnitCombatReach(target) + 1.5)
	else
		-- Unlockers wich dont have UnitCombatReach like functions...
		return NeP.Core.Distance('pet', target) < 5
	end
end)

NeP.DSL.RegisterConditon("inMelee", function(target)
   return NeP.Core.UnitAttackRange('player', target, 'melee')
end)

NeP.DSL.RegisterConditon("inRanged", function(target)
   return NeP.Core.UnitAttackRange('player', target, 'ranged')
end)

NeP.DSL.RegisterConditon("power.regen", function(target)
  return select(2, GetPowerRegen(target))
end)

NeP.DSL.RegisterConditon("casttime", function(target, spell)
    local name, rank, icon, cast_time, min_range, max_range = GetSpellInfo(spell)
    return cast_time
end)

NeP.DSL.RegisterConditon("castwithin", function(target, spell)
	local SpellID = select(7, GetSpellInfo(spell))
	for k, v in pairs( NeP.ActionLog.log ) do
		local id = select(7, GetSpellInfo(v.description))
		if (id and id == SpellID and v.event == "Spell Cast Succeed") or tonumber( k ) == 20 then
			return tonumber( k )
		end
	end
	return 20
end)

NeP.DSL.RegisterConditon('twohand', function(target)
  return IsEquippedItemType("Two-Hand")
end)

NeP.DSL.RegisterConditon('onehand', function(target)
  return IsEquippedItemType("One-Hand")
end)